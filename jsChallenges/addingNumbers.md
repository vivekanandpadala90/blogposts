# Adding Numbers

## Challenge: Add up all the numbers from 1 to num. For example: if the input is 5 then your program should return 10 because 1 + 2 + 3 + 4 + 5 = 15. Here the num value is 5.

### Solution with comments:

``` javascript
// Initializing a function named totalSum.
function totalSum(num) {
  // Initializing a variable named result and assigning it a value of 0.
    var result = 0;
    // Looping values from 1 to the value lessthan or equal to the value of num.
    for (var i = 1; i <= num; i++) { 
    // Adding up values from 1 to num to the result and storing the value in result.    
        result = result + i;
    }
    // Returning the final value in the result.
    return result;
}
// Calling the function totalSum() by assigning a value of 5 to num so that it will add all the numbers from 1 to 5 and return the result as 15.
totalSum(5);
```
### Solution without comments:

``` javascript
function totalSum(num) {
    var result = 0;
    for (var i = 1; i <= num; i++) { 
        result = result + i;
    }
    return result;
}
totalSum(5);
```
